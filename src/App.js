import React from 'react';
import './App.less';
import TodoList from './components/TodoList';
import Button from './components/Button';

const appInitState = {
  appRenderFlag: 0,
  showButtonFlag: 0,
  showButtonText: 'Show',
  refreshButtonText: 'Refresh',
  content: [],
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = appInitState;
    this.showButtonClickHandler = this.showButtonClickHandler.bind(this);
    this.refreshButtonClickHandler = this.refreshButtonClickHandler.bind(this);
    this.addButtonClickHandler = this.addButtonClickHandler.bind(this);
  }

  render() {
    return (
      <div className='App'>
        <div className='BtnSet'>
          <Button text={this.state.showButtonText} handler={this.showButtonClickHandler}/>
          <Button text={this.state.refreshButtonText} handler={this.refreshButtonClickHandler}/>
        </div>
        {this.state.showButtonFlag ? <TodoList content={this.state.content} addButtonClick={this.addButtonClickHandler}/> : null}
      </div>
    );
  }

  showButtonClickHandler() {
    let option = ['Hide', 'Show'];

    this.setState({
      showButtonText: option[this.state.showButtonFlag],
      showButtonFlag: +!this.state.showButtonFlag,
    });
  }

  refreshButtonClickHandler() {
    this.setState({
      appRenderFlag: +!this.state.appRenderFlag,
    });
  }

  addButtonClickHandler() {
    let content = this.state.content;

    content.push(`List Title ${content.length+1}`);
    this.setState({ content });
  }
}

export default App;