import React from 'react';
import './Button.less';

const Button = (props) => {
  return (
    <div>
      <button type='button' onClick={props.handler}>{props.text}</button>
    </div>
  );
}

export default Button;