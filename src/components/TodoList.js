import React, {Component} from 'react';
import './TodoList.less';
import Button from './Button';

const ListItems = (props) => {
  const items = props.content.map((item, index) => <li className='list-item' key={index}>{item}</li>);

  return (
    <ul>
      {items}
    </ul>
  );
};

class TodoList extends Component {
  constructor(props) {
    console.log("constructor");
    super(props);
  }

  render() {
    console.log("render");
    return (
      <div className='TodoList'>
        <Button text='Add' handler={this.props.addButtonClick}/>
        <ListItems content={this.props.content} />
      </div>
    );
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentDidUpdate() {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }
}

export default TodoList;
